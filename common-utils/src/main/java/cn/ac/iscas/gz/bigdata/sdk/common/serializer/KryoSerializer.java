package cn.ac.iscas.gz.bigdata.sdk.common.serializer;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;

/**
 * @Title:
 *  Kryo对象序列化工具
 * @description:
 *
 * @Company: iscas
 * @version 1.0
 */
public class KryoSerializer<T> implements ISerializer<T>,Serializable {
    
    private static final long serialVersionUID = 1L;
    
    // 日志
    private static Logger log = LogManager.getLogger(cn.ac.iscas.gz.bigdata.sdk.common.serializer.KryoSerializer.class);

    
    private Kryo kryo = null;

	public KryoSerializer() {
		kryo = new Kryo();
		kryo.setReferences(false);
		kryo.setRegistrationRequired(false);
	}

	@Override
	public byte[] serialize(T obj){
	    if(obj == null){
	        return null;
	    }
		try{
			return serialize(obj, -1);
		}catch(Throwable tr){
		    log.error("serialize catch an exception",tr);
		}
		
		return null;
	}

	public synchronized byte[] serialize(T obj, int maxBufferSize) {
	    if(obj == null){
	        return null;
	    }
		Output output = new Output(1024, maxBufferSize);
		kryo.writeClassAndObject(output, obj);
		return output.toBytes();
	}

	@Override
	public synchronized T unserialize(byte[] bytes){
	    if(bytes == null || bytes.length <= 0){
	        return null;
	    }
	    
		try{
			Input input = new Input(bytes);
			return (T)kryo.readClassAndObject(input);
		}catch(Throwable tr){
		    log.error("unserialize catch an exception",tr);
		}
		
		return null;
	}

}
