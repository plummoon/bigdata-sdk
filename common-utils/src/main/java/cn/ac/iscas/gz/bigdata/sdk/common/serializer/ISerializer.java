package cn.ac.iscas.gz.bigdata.sdk.common.serializer;
/**
 * @Title:
 *   对象序列化接口
 * @description:
 *
 * @Company: iscas
 * @version 1.0
 */
public interface ISerializer<T> {
	
	/**
	 * 将对象序列化为byte[]
	 * @param obj
	 * @return
	 */
	public byte[] serialize(T obj);
	
	/**
	 * 将byte数组反序列化为对象
	 * @param bytes
	 * @return
	 */
	public T unserialize(byte[] bytes); 

}
