#Druid服务使用说明

##1 新增配置文件
druidx_config.properties
```
druidx_protocol=19.14.133.97
druidx_host=19.14.133.97
druidx_port=80
druidx_endpoint=druid/v2
druidx_concurrentconnections=8
```
##2 Client使用方法
添加依赖

```
<dependency>
    <groupId>cn.ac.iscas.gz.bigdata</groupId>
    <artifactId>bigdata-sdk</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```

```
import cn.ac.iscas.gz.bigdata.sdk.druidio.DruidXClient

......
//获取客户端
druidClient = DruidXClient.getInstance();
druidClient.getDruidJerseyClient().connect();

//查询条件Build
DruidSearchQuery query = DruidSearchQuery.builder()
                .dataSource(new TableDataSource("ds_drug_test_lee_001"))
                .granularity(new SimpleGranularity(PredefinedGranularity.DAY))
                .virtualColumns(Collections.singletonList(new ExpressionVirtualColumn("dim3", "BATCH + PACKSPEC", OutputType.STRING)))
                .searchDimensions(searchDimensions)
                .query(searchQuerySpec)
                .sort(SortingOrder.LEXICOGRAPHIC)
                .intervals(Collections.singletonList(interval))
                .build();
//业务查询
druidClient.getDruidJerseyClient().query(query)
......

```

