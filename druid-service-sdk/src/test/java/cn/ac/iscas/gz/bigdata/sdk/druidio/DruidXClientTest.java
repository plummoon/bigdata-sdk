package cn.ac.iscas.gz.bigdata.sdk.druidio;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import in.zapr.druid.druidry.client.exception.ConnectionException;
import in.zapr.druid.druidry.dataSource.TableDataSource;
import in.zapr.druid.druidry.dimension.DruidDimension;
import in.zapr.druid.druidry.dimension.SimpleDimension;
import in.zapr.druid.druidry.dimension.enums.OutputType;
import in.zapr.druid.druidry.filter.searchQuerySpec.InsensitiveContainsSearchQuerySpec;
import in.zapr.druid.druidry.filter.searchQuerySpec.SearchQuerySpec;
import in.zapr.druid.druidry.granularity.PredefinedGranularity;
import in.zapr.druid.druidry.granularity.SimpleGranularity;
import in.zapr.druid.druidry.query.config.Interval;
import in.zapr.druid.druidry.query.config.SortingOrder;
import in.zapr.druid.druidry.query.search.DruidSearchQuery;
import in.zapr.druid.druidry.virtualColumn.ExpressionVirtualColumn;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import org.json.JSONException;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DruidXClientTest {

    private static ObjectMapper objectMapper;

    private DruidXClient druidClient;

    @BeforeClass
    public void init() throws ConnectionException {
        objectMapper = new ObjectMapper();
        druidClient = DruidXClient.getInstance();
        druidClient.getDruidJerseyClient().connect();
    }

    @Test
    public void testSampleQuery() throws JsonProcessingException, JSONException {

        List<DruidDimension> searchDimensions
                = Arrays.asList(new SimpleDimension("BATCH"), new SimpleDimension("PACKSPEC"));

        SearchQuerySpec searchQuerySpec = new InsensitiveContainsSearchQuerySpec("袋");

        DateTime startTime = new DateTime(2018, 12, 1, 0,
                0, 0, DateTimeZone.UTC);
        DateTime endTime = new DateTime(2020, 1, 3, 0,
                0, 0, DateTimeZone.UTC);
        Interval interval = new Interval(startTime, endTime);

        DruidSearchQuery query = DruidSearchQuery.builder()
                .dataSource(new TableDataSource("ds_drug_test_lee_001"))
                .granularity(new SimpleGranularity(PredefinedGranularity.DAY))
                .virtualColumns(Collections.singletonList(new ExpressionVirtualColumn("dim3", "BATCH + PACKSPEC", OutputType.STRING)))
                .searchDimensions(searchDimensions)
                .query(searchQuerySpec)
                .sort(SortingOrder.LEXICOGRAPHIC)
                .intervals(Collections.singletonList(interval))
                .build();
        try{

            String actualJson = objectMapper.writeValueAsString(query);
            if(StringUtils.isEmpty(actualJson)){
                System.out.println("actualJson is null");
            }

            String result =  druidClient.getDruidJerseyClient().query(query);
            if(StringUtils.isEmpty(result)){
                System.out.println("result is null");
            }

        }catch (in.zapr.druid.druidry.client.exception.QueryException ex){
            System.out.println(ex);
        }

    }
}
