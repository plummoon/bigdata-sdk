package cn.ac.iscas.gz.bigdata.sdk.druidio;

import cn.ac.iscas.gz.bigdata.sdk.common.CommonIOUtils;
import in.zapr.druid.druidry.client.DruidClient;
import in.zapr.druid.druidry.client.DruidConfiguration;
import in.zapr.druid.druidry.client.DruidJerseyClient;
import in.zapr.druid.druidry.client.DruidQueryProtocol;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;

import java.io.InputStream;
import java.util.Properties;

public class DruidXClient{
    protected final static Logger log = LogManager.getLogger(cn.ac.iscas.gz.bigdata.sdk.druidio.DruidXClient.class);

    private  DruidJerseyClient druidJerseyClient = null;

    private static final String CONFIG_FILE = "druidx_config.properties"; //db参数配置文件
    private static final Integer DEFAULT_PORT = 8082;
    private static final Integer DEFAULT_CONCURRENTCONNECTIONS = 8;

    private static DruidXClient _instance = null;
    private static DruidConfiguration druidConfiguration = null;
    private static ClientConfig customJerseyConfig = null;

    private DruidXClient(){
        try{
            initDruidClient();
        }catch (Throwable tr){
            log.error("initDruidClient catch an exception",tr);
        }
    }

    private void initDruidClient(){
        boolean loadFlag = this.loadConfigFromLocalProperties();
        if(loadFlag == false){
            log.error("load properties from config loader error!!!");
        }

        String druidioProtocol = System.getProperty("druidx_protocol");
        DruidQueryProtocol druidQueryProtocol = DruidQueryProtocol.HTTP;
        if(druidioProtocol.equalsIgnoreCase(DruidQueryProtocol.HTTPS.toString())){
            druidQueryProtocol=DruidQueryProtocol.HTTPS;
        }

        String druidxHost = System.getProperty("druidx_host");

        Integer druidxPort = this.DEFAULT_PORT;
        if(System.getProperty("druidx_port") == null){
            log.warn("druidx_port properties not exist!!!");
        }else{
            druidxPort = Integer.valueOf(System.getProperty("druidx_port"));
        }

        Integer druidxConcurrentConnections= this.DEFAULT_CONCURRENTCONNECTIONS;
        if(System.getProperty("druidx_concurrentconnections") == null){
            log.warn("druidx_concurrentconnections properties not exist!!!");
        }else{
            druidxConcurrentConnections = Integer.valueOf(System.getProperty("druidx_concurrentconnections"));
        }

        String druidxEndpoint = System.getProperty("druidx_endpoint");

        //read configuration
        druidConfiguration = DruidConfiguration
                .builder()
                .protocol(druidQueryProtocol)
                .host(druidxHost)
                .port(druidxPort)
                .endpoint(druidxEndpoint)
                .concurrentConnectionsRequired(druidxConcurrentConnections)
                .build();

        druidJerseyClient = new DruidJerseyClient(druidConfiguration, customJerseyConfig);
    }

    /**
     * load config from local properties
     * @return
     */
    private boolean loadConfigFromLocalProperties() {
        InputStream is = null;
        try {
            is = cn.ac.iscas.gz.bigdata.sdk.druidio.DruidXClient.class.getClassLoader().getResourceAsStream(CONFIG_FILE);

            if(is == null){
                log.warn("can't get "+CONFIG_FILE+" in classpath");
                return false;
            }

            Properties props = new Properties();
            props.load(is);

            if(props == null || props.size() <= 0){
                return false;
            }

            System.getProperties().putAll(props);
            log.info(CONFIG_FILE+props.toString()+" to System.properties");

            return true;
        } catch (Exception ex) {
            log.warn("load "+CONFIG_FILE+" catch an exception",ex);
            return false;
        }finally{
            CommonIOUtils.close(is);
        }
    }

    public static synchronized DruidXClient getInstance(){
        if(_instance == null){
            _instance = new DruidXClient();;
        }
        return _instance;
    }

    public DruidJerseyClient getDruidJerseyClient() {
        return druidJerseyClient;
    }
}
