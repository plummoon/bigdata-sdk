#Druid服务使用说明

##1 新增配置文件
mq_config.properties
```
zkconnect=127.0.0.1:52181
kafka_brokerlist=127.0.0.1:9092
kafka_zkroot=kafka
```
##2 Client使用方法
添加依赖

```
<dependency>
    <groupId>cn.ac.iscas.gz.bigdata</groupId>
    <artifactId>mq-service-sdk</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```

#####生产消息
```
import cn.ac.iscas.gz.bigdata.sdk.mq.MessageClient
import cn.ac.iscas.gz.bigdata.sdk.mq.Message
......
public static void main(String[] args) {
    //获取客户端
    MessageClient.getInstance();
    
    //初始化message
    final Message message = new Message();
    Map userObj = this.buildMsgData(i);
    message.setUserObject(userObj);
    message.setTopic("test");
    
    //发送消息
    MessageClient.getInstance().send(message);
}
......

    
/**
 * 构建消息数据
 * @param i
 * @return
 */
private Map<String, Object> buildMsgData(int i) throws JsonProcessingException {
    Map<String, Object> map = new LinkedHashMap<String, Object>();
    map.put("filed1_"+i, "value1_"+i);
    map.put("filed2_"+i, "value2_"+i);
    map.put("filed3_"+i, "value3_"+i);
    map.put("filed4_"+i, "value4_"+i);
    map.put("filed5_"+i, "value5_"+i);
    map.put("filed5_"+i, "value5_"+i);
    
    Map<String,Object> subObj = new HashMap<String,Object>();
    subObj.put("s1", "1");
    subObj.put("s2", "2");
    map.put("subMap", objectMapper.writeValueAsString(subObj));
    return map;
}

```
#####消费消息
```
import cn.ac.iscas.gz.bigdata.sdk.mq.MessageClient
import cn.ac.iscas.gz.bigdata.sdk.mq.Message

public static void main(String[] args) {
    MessageClient.getInstance();
    //topic_name为需要消费的主题名称
    MessageClient.getInstance().addMessageHandler("topic_name", msg -> {
        try{
            if(msg == null){
                log.error("msg is null");
            }else{
                log.info("recieved a message "+msg.toString());
            }
        }catch(Throwable tr){
            tr.printStackTrace();
        }
    });
}


