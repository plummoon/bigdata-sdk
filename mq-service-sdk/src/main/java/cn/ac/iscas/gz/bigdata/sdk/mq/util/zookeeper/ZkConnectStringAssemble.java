package cn.ac.iscas.gz.bigdata.sdk.mq.util.zookeeper;

import cn.ac.iscas.gz.bigdata.sdk.common.StringUtils;

import java.util.Iterator;
import java.util.List;


/**
 * @Title:
 *  zookeeper连接串组装器
 * @description:
 *
 * @Company: iscas
 * @version 1.0
 */
public class ZkConnectStringAssemble {
	
	/**
	 * 获取zookeeper连接串
	 * @param zkServers
	 * @param zkPort
	 * @return
	 * @throws Exception
	 */
	public static String getZkConnectString(List<String> zkServers,String zkPort){
		if(zkServers == null || zkServers.size() <= 0 || StringUtils.isEmpty(zkPort)){
			return null;
		}
		
		Iterator<String> it = zkServers.iterator();
		StringBuffer sb = new StringBuffer();
		while (it.hasNext()){
			sb.append(it.next());
			sb.append(":");
			sb.append(zkPort);
			if (it.hasNext()){
				sb.append(",");
			}
		}
		return sb.toString();
	}

}
