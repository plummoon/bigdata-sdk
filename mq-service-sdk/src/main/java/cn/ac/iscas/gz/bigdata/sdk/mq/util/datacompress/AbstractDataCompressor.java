package cn.ac.iscas.gz.bigdata.sdk.mq.util.datacompress;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @Title:
 *
 * @description:
 *
 * @Company: iscas
 * @version 1.0
 */
public abstract class AbstractDataCompressor implements IDataCompressor {
    
    //日志
    private static Logger log = LogManager.getLogger(AbstractDataCompressor.class);
    
    //字符集
    public static final String charSet = "utf-8"; 
    
    public AbstractDataCompressor(){
    }

    /**
     * 数据压缩
     * @param data 待压缩数据
     * @return  压缩后数据
     */
    @Override
	public abstract byte[] compress(byte[] data);

    /**
     * 数据压缩
     * @param data 待压缩字符串
     * @return  压缩后字符串
     */
    @Override
	public abstract byte[] decompress(byte[] data);
    
    /**
     * 数据压缩
     * @param data 待压缩字符串
     * @return  压缩后字符串
     */
    @Override
	public String compress(String str){
        if(str == null || str.length() == 0){
            return str;
        }
        
        try {
            byte[] data = str.getBytes(charSet);
            byte[] bytes = this.compress(data);
            String result = new String(bytes,charSet);
            return result;
        } catch (Exception ex) {
            log.error("compress str catch an exception",ex);
        }
        
        return null;
    }

    /**
     * 数据解压
     * @param data 待解压字符串
     * @return 解压后的字符串
     */
    @Override
	public String decompress(String str){
        if(str == null || str.length() == 0){
            return str;
        }
        
        try {
            byte[] data = str.getBytes(charSet);
            byte[] bytes = this.decompress(data);
            String result = new String(bytes,charSet);
            return result;
        } catch (Exception ex) {
            log.error("decompress str catch an exception",ex);
        }
        
        return null;
    }

}
