package cn.ac.iscas.gz.bigdata.sdk.mq.kafka;

import cn.ac.iscas.gz.bigdata.sdk.common.StringUtils;
import cn.ac.iscas.gz.bigdata.sdk.mq.Message;
import org.apache.kafka.clients.producer.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Properties;

/**
 * @Title:
 *  kafka消息发送者
 * @description:
 *  负责Kafka消息发送
 * @Company: iscas
 * @version 1.0
 */
public class KafkaMessageProducer<T> {
    
    //日志
    private static final Logger log = LogManager.getLogger(cn.ac.iscas.gz.bigdata.sdk.mq.kafka.KafkaMessageProducer.class);
    
    //配置参数
    private Properties props = null;
    
    //broker地址列表
    private String brokerlist = null;
    
    private Producer<String, T> producer = null;

    /**
     * 配置属性
     * @param props
     * @throws Exception
     */
    public KafkaMessageProducer(Properties props) throws Exception{
        if(props == null || props.size() <= 0){
            throw new RuntimeException("props can't be null");
        }
        
//        String brokerlistStr = props.getProperty("metadata.broker.list");
        String brokerlistStr = props.getProperty("bootstrap.servers");
        if(StringUtils.isEmpty(brokerlistStr)){
            throw new RuntimeException("brokerlistStr can't be null");
        }
        
        this.props = props;
        this.brokerlist = brokerlistStr;
        
        this.createProducer();
    }

    
    /**
     * 构建生产者
     */
    private synchronized void createProducer() throws Exception{
        try{
            log.info("begining kafka createProducer for brokerlist="+this.brokerlist);
            this.producer = new KafkaProducer<>(this.props);
            
            log.info("kafka createProducer for brokerlist="+this.brokerlist+" successfully!");
        }catch(Throwable tr){
            log.error("kafka createProducer for brokerlist="+this.brokerlist+" catch an exception",tr);
        }
    }

    /**
     * 发送消息到Metaq服务器
     * @param msgBean 消息对象
     * @throws Exception
     */
    public void send(Message msgBean) throws Exception{
        if(msgBean == null){
            return;
        }
        String topicName = msgBean.getTopic();
        this.send(topicName, msgBean);
    }
    
    /**
     * 发送消息到服务器
     * @param topicName topic名称
     * @param msgBean 消息对象
     * @throws Exception
     */
    public void send(String topicName, Message msgBean) throws Exception{
        this.send(topicName, msgBean, false);
    }
    
    
    
    /**
     * 发送消息到Metaq服务器
     * @param msgBean 消息对象
     * @param isOrdered 是否顺序发送  true:是顺序发送，false:不保证顺序
     * @throws Exception
     */
    public void send(Message msgBean,boolean isOrdered) throws Exception{
        if(msgBean == null){
            return;
        }
        String topicName = msgBean.getTopic();
        this.send(topicName, msgBean,isOrdered);
    }
    
    /**
     * 发送消息到服务器
     * @param topicName topic名称
     * @param msgBean 消息对象
     * @param isOrdered 是否顺序发送  true:是顺序发送，false:不保证顺序
     * @throws Exception
     */
    public void send(String topicName, Message msgBean,boolean isOrdered) throws Exception{
        if(topicName == null || topicName.trim().equals("")){
            throw new Exception("topicName can't be null");
        }

        //文本类型
        ProducerRecord<String, T> keyedMsg;
        if("1".equals(props.getProperty("kafka_message_plaintext"))){
            keyedMsg = new ProducerRecord<String, T>(topicName, msgBean.getMessageKey(), (T) msgBean.getUserObject());
        }else{
            keyedMsg = new ProducerRecord<String, T>(topicName, msgBean.getMessageKey(), (T) msgBean);
        }
        producer.send(keyedMsg,
                (metadata, e) -> {
                    if (e != null) {
                        e.printStackTrace();
                        log.error(e.getMessage());
                    } else {
                        log.debug("The offset of the record we just sent is: " + metadata.offset());
                    }
                });
    }
    
    /**
     * 获取顺序发送的key
     * @param topicName
     * @return
     */
    private String getOrderedKey(String topicName){
        return "ordered_"+topicName;
    }
}

