package cn.ac.iscas.gz.bigdata.sdk.mq.kafka.encoder;

import cn.ac.iscas.gz.bigdata.sdk.common.serializer.ISerializer;
import cn.ac.iscas.gz.bigdata.sdk.common.serializer.SerializerFactory;
import cn.ac.iscas.gz.bigdata.sdk.mq.Message;
import cn.ac.iscas.gz.bigdata.sdk.mq.util.datacompress.DataCompressorFactory;
import cn.ac.iscas.gz.bigdata.sdk.mq.util.datacompress.IDataCompressor;
import net.sf.json.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

/**
 * @Title:
 *  Kafka消息序列化封装
 * @description:
 *
 * @Company: iscas
 * @version 1.0
 */
public class KafkaMessageSerializer implements org.apache.kafka.common.serialization.Serializer<Message> {
    
    private static Logger log = LogManager.getLogger(KafkaMessageSerializer.class);

    //数据压缩器
    private IDataCompressor dataCompressor = null;
    //序列化器
    private ISerializer serializer = null;

    public String charSet = "utf-8";
    
    public KafkaMessageSerializer(){
        dataCompressor = DataCompressorFactory.createDataCompressor();
    }

    public byte[] toBytes(Message msg) {
        byte[] newBytes = null;
        try {
            Object userObj = msg.getUserObject();
            String userObjClass =  msg.getUserObjectClass();

            JSONObject jsonObj = JSONObject.fromObject(msg);
            String jsonString = jsonObj.toString();

            byte[] bytes = jsonString.getBytes(charSet);

            int compressFlag = 0;
            if(msg.isCompress()){ //数据需要压缩
                bytes = this.dataCompressor.compress(bytes);
                compressFlag = 1;
            }

            newBytes = new byte[bytes.length + 1];
            newBytes[0] = intToByte(compressFlag);
            System.arraycopy(bytes, 0, newBytes, 1, bytes.length);

            log.info("toBytes bytes="+newBytes.length);
        } catch (Exception e) {
            log.error("serialize catch an exception",e);
        }
        return newBytes;
    }

    public byte intToByte(int x) {
        return (byte) x;
    }

    @Override
    public byte[] serialize(String s, Message message) {
        return toBytes(message);
    }

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {

    }

    @Override
    public void close() {

    }
}

