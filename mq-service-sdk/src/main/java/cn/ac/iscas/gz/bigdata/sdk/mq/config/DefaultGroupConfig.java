package cn.ac.iscas.gz.bigdata.sdk.mq.config;

import cn.ac.iscas.gz.bigdata.sdk.common.InetAddressUtil;
import cn.ac.iscas.gz.bigdata.sdk.common.StringUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * @Title:
 *   默认分组配置
 * @description:
 * 
 * @Company: iscas
 * @author ly
 * @version 1.0
 */
public class DefaultGroupConfig {
    
    //日志
    private static Logger log = LogManager.getLogger(cn.ac.iscas.gz.bigdata.sdk.mq.config.DefaultGroupConfig.class);
    
    //默认分组前缀
    public static final String DEFAULT_GROUP_PREFIEX = "iscas";
    
    private String defaultGroup = null;
    
    private static cn.ac.iscas.gz.bigdata.sdk.mq.config.DefaultGroupConfig instance = new cn.ac.iscas.gz.bigdata.sdk.mq.config.DefaultGroupConfig();
    
    public static cn.ac.iscas.gz.bigdata.sdk.mq.config.DefaultGroupConfig getInstance(){
        return instance;
    }
    
    private DefaultGroupConfig(){
        this.init();
    }
    
    private void init(){
        this.defaultGroup = this.assembleDefaultGroup();
    }
    
    /**
     * @return the defaultGroup
     */
    public String getDefaultGroup() {
        return defaultGroup;
    }

    /**
     * 获取默认的分组
     * @return
     */
    private String assembleDefaultGroup(){
    	String defaultGroup = System.getProperty("mq-defaultgroup");
        if(StringUtils.isEmpty(defaultGroup)){
            defaultGroup = DEFAULT_GROUP_PREFIEX;
        }

        String ip = InetAddressUtil.getLocalIp();
        String systemID = System.getProperty("systemId");
        if(StringUtils.isEmpty(systemID)){
        	systemID = "unnamed";
        }
        
        defaultGroup = defaultGroup + "_" + systemID+"_"+ip;
        return defaultGroup;
    }
}
