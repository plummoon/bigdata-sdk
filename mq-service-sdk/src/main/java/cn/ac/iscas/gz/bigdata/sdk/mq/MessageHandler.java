
package cn.ac.iscas.gz.bigdata.sdk.mq;



/**
 * 消息处理回调接口
 * @author ly
 */
public interface MessageHandler {
    /**
     * 消息处理回调方法
     * @param msg 消息对象
     */
    void onMessage(Message msg);
    
}
