package cn.ac.iscas.gz.bigdata.sdk.mq;

import cn.ac.iscas.gz.bigdata.sdk.common.CommonIOUtils;
import cn.ac.iscas.gz.bigdata.sdk.common.StringUtils;
import cn.ac.iscas.gz.bigdata.sdk.mq.config.DefaultGroupConfig;
import cn.ac.iscas.gz.bigdata.sdk.mq.kafka.KafkaMessageClient;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.util.Properties;


/**
 * @Title:
 *  Message客户端工厂类
 * @description:
 *
 * @version 1.0
 */
public class MessageClientFactory {
    
    private static Logger log = LogManager.getLogger(MessageClientFactory.class);
    
    private static final String CONFIG_FILE = "mq_config.properties"; //db参数配置文件
    
	public static final String TYPE_KAFKA = "kafka";
	
	//使用的消息服务器类型,为以后其他服务器类型预留准备
	private String type = TYPE_KAFKA;
	
	//消息操作客户端
	private MessageClient messageClient = null;
	
	//单例
	public static cn.ac.iscas.gz.bigdata.sdk.mq.MessageClientFactory instance = null;
	
	//默认分组前缀
	public static final String DEFAULT_GROUP_PREFIEX = "iscas";
	

	
	public synchronized static cn.ac.iscas.gz.bigdata.sdk.mq.MessageClientFactory getInstance(){
		if(instance == null){
			instance = new cn.ac.iscas.gz.bigdata.sdk.mq.MessageClientFactory();
		}
		
		return instance;
	}
	
	private MessageClientFactory(){
		try{
			initMessageClient();
		}catch(Throwable tr){
			log.error("initMessageClient catch an exception",tr);
		}
	}
	
	/**
	 * 初始化客户端实例
	 * @throws Exception
	 */
	private void initMessageClient() throws Exception{
		boolean loadFlag = this.loadConfigFromLocalProperties();
		if(loadFlag == false){
			log.info("load properties from config loader");
			//TODO
		}
		
        String msgservertype = System.getProperty("msgservertype");
        log.info("----------msgservertype="+msgservertype+"-------------");
        if(msgservertype != null && msgservertype.trim().equalsIgnoreCase(TYPE_KAFKA)){
            this.type = TYPE_KAFKA;
            messageClient = this.createKafkaMessageClient();
        }else{
        	this.type = TYPE_KAFKA;
            messageClient = this.createKafkaMessageClient();
        }
	}
	
	
	/**
	 * 从本地配置文件中获取sdk-mq-client相关配置
	 * @return
	 */
	private boolean loadConfigFromLocalProperties(){
		InputStream is = null;
        try {
            is = cn.ac.iscas.gz.bigdata.sdk.mq.MessageClientFactory.class.getClassLoader().getResourceAsStream(CONFIG_FILE);
            
            if(is == null){
                log.warn("can't get "+CONFIG_FILE+" in classpath");
                return false;
            }

            Properties props = new Properties();
            props.load(is);
            
            if(props == null || props.size() <= 0){
            	return false;
            }
            
            System.getProperties().putAll(props);
            log.info("put mq_config.properties "+props.toString()+" to System.properties");
            
            return true;
        } catch (Exception ex) {
            log.warn("load "+CONFIG_FILE+" catch an exception",ex);
            return false;
        }finally{
            CommonIOUtils.close(is);
        }
	}
	
	
	/**
     * 构建kafka消息服务器客户端
     * @return
     */
    private KafkaMessageClient createKafkaMessageClient() throws Exception{
        String zkConnect = System.getProperty("kafka_zkconnect");
        if(zkConnect == null || zkConnect.trim().equals("")){
        	zkConnect = System.getProperty("zkconnect");
        }
        
        String brokerlist = System.getProperty("kafka_brokerlist");
        String zkRoot = System.getProperty("kafka_zkroot");
        String plainText = System.getProperty("kafka_message_plaintext");
        String defaultGroup = DefaultGroupConfig.getInstance().getDefaultGroup();
        log.info("-------------kafka address "+brokerlist+",zkRoot="+zkRoot+",defaultGroup="+defaultGroup+"----------------");
        KafkaMessageClient kafkaClient;
        if(StringUtils.isEmpty(plainText)){
        	plainText = "0";
		}
        if(plainText.equals("1")){
			kafkaClient = new KafkaMessageClient<String>(zkConnect,brokerlist,defaultGroup,zkRoot,plainText);
		}
        else{
			kafkaClient = new KafkaMessageClient<Message>(zkConnect,brokerlist,defaultGroup,zkRoot);
		}
        
        return kafkaClient;
    }
    
	
	
	public MessageClient getMessageClient() {
		return messageClient;
	}


}
