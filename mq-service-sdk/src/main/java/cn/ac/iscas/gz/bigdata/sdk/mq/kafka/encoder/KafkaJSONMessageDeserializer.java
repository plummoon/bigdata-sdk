package cn.ac.iscas.gz.bigdata.sdk.mq.kafka.encoder;

import cn.ac.iscas.gz.bigdata.sdk.mq.Message;
import cn.ac.iscas.gz.bigdata.sdk.mq.util.datacompress.DataCompressorFactory;
import cn.ac.iscas.gz.bigdata.sdk.mq.util.datacompress.IDataCompressor;
import net.sf.ezmorph.bean.MorphDynaBean;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @Title:
 *   Kafka的消息json序列化器
 * @description:
 *
 * @Company: iscas
 * @version 1.0
 */
public class KafkaJSONMessageDeserializer implements org.apache.kafka.common.serialization.Deserializer<Message> {
    
    private static Logger log = LogManager.getLogger(KafkaJSONMessageDeserializer.class);

    public String charSet = "utf-8";
    
    public static final String KEY_USEROBJECTCLASS = "userObjectClass";
    
    public static final String KEY_USEROBJECT = "userObject";
    
	//数据压缩器
	private IDataCompressor dataCompressor = null;
    
    public KafkaJSONMessageDeserializer(){
    	dataCompressor = DataCompressorFactory.createDataCompressor();
    }

    
    public Message toMessage(byte[] newBytes){
    	try{
    		int compressFlag = byteToInt(newBytes[0]);
    		byte[] bytes = new byte[newBytes.length-1];
    		System.arraycopy(newBytes, 1,bytes, 0, newBytes.length-1);
    		if(compressFlag == 1){ //是压缩的
    			bytes = this.dataCompressor.decompress(bytes);
    		}
    		
	    	String jsonString =  new String(bytes,charSet);
	    	log.debug("toMessage jsonString="+jsonString);
	    	JSONObject jsonObj = JSONObject.fromObject(jsonString);

	    	String userObjectClassName = null;
	    	String itemClassName = null;
	    	if(jsonObj.has(KEY_USEROBJECTCLASS)){
	    		userObjectClassName = jsonObj.getString(KEY_USEROBJECTCLASS);
	    	}

	    	if(userObjectClassName == null || userObjectClassName.trim().equals("")){
	    		userObjectClassName = HashMap.class.getName();
	    	}
	    	
	    	if(userObjectClassName.indexOf("<") != -1){
	    		int indexStart = userObjectClassName.indexOf("<");
	    		int indexEnd = userObjectClassName.indexOf(">");
	    		itemClassName = userObjectClassName.substring(indexStart+1,indexEnd);
	    		userObjectClassName = userObjectClassName.substring(0, indexStart);
	    	}
	    	
	    	Message message = null;
	    	Object userObj = null;
	    	Class userObjClass = Class.forName(userObjectClassName);
	    	if(java.util.Collection.class.isAssignableFrom(userObjClass)){ //集合
	    		JSONArray jsonArray = jsonObj.getJSONArray(KEY_USEROBJECT);
	    		if(itemClassName == null || itemClassName.trim().equals("")){
	    			itemClassName = HashMap.class.getName();
	    		}
	    		Class itemClass = Class.forName(itemClassName);
	    		userObj = jsonArray.toCollection(jsonArray, itemClass);
	    		jsonObj.remove(KEY_USEROBJECT);
	    		message = (Message)JSONObject.toBean(jsonObj, Message.class);
	    		if(message == null){
	    			return null;
	    		}
	    		message.setUserObject(userObj);
	    	}else{
	    		Map classMap = new HashMap();
	    		classMap.put("userObject", userObjClass);
	    		message = (Message)JSONObject.toBean(jsonObj, Message.class, classMap);
	    		if(message == null){
	    			return null;
	    		}
	    		userObj = message.getUserObject();
	    	}
	    	
    		if(userObj instanceof Map){ //做Map嵌套处理，处理MorphDynaBean
    			Map userObjMap = (Map)userObj;
    			Map subMap = new HashMap();
    			Iterator<Map.Entry> it = userObjMap.entrySet().iterator();
    			while(it.hasNext()){
    				Map.Entry entry = it.next();
    				Object key = entry.getKey();
    				Object value = entry.getValue();
    				if(value == null){
    					continue;
    				}
    				
    				if(value instanceof MorphDynaBean){
    					String valueJsonStr = JSONObject.fromObject(value).toString();
    					subMap.put(key, valueJsonStr);
    				}
    			}
    			
    			userObjMap.putAll(subMap);
    		}
    		
	    	return message;
    	}catch(Throwable tr){
    		log.error("toMessage catch an exception",tr);
    	}
    	
    	return null;
    }
    
    public byte intToByte(int x) {  
        return (byte) x;  
    }  
      
    public int byteToInt(byte b) {  
        //Java 总是把 byte 当做有符处理；我们可以通过将其和 0xFF 进行二进制与得到它的无符值  
        return b & 0xFF;  
    }  
    
    
    public static void main(String [] args){
    	KafkaJSONMessageDeserializer test = new KafkaJSONMessageDeserializer();
    	//测试 int 转 byte  
    	int int0 = 1;  
    	byte byte0 = test.intToByte(int0);  
    	System.out.println("byte0=" + byte0);//byte0=-22  
    	//测试 byte 转 int  
    	int int1 = test.byteToInt(byte0);  
    	System.out.println("int1=" + int1);//int1=234  
    }

	@Override
	public void configure(Map<String, ?> configs, boolean isKey) {

	}

	@Override
	public Message deserialize(String s, byte[] bytes) {
		return toMessage(bytes);
	}

	@Override
	public void close() {

	}
}
