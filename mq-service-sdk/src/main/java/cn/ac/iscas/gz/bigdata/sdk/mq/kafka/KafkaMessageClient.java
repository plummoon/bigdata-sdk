package cn.ac.iscas.gz.bigdata.sdk.mq.kafka;

import cn.ac.iscas.gz.bigdata.sdk.mq.Message;
import cn.ac.iscas.gz.bigdata.sdk.mq.MessageClient;
import cn.ac.iscas.gz.bigdata.sdk.mq.MessageHandler;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.NamingException;
import java.util.Properties;


/**
 * @Title:
 *    Kafka消息处理器
 * @description:
 *
 * @Company: iscas
 * @version 1.0
 */
public class KafkaMessageClient<T> extends MessageClient {
    
    private static final long serialVersionUID = 1L;
    
    //日志
    private static Logger log = LogManager.getLogger(cn.ac.iscas.gz.bigdata.sdk.mq.kafka.KafkaMessageClient.class);
    
    //zkConnect
    private String zkConnect = null;
    
    private String brokerList = null;
    
    //默认消息分组
    private String defaultGroup = null;

    private String kafkaMessagePlaintext = "0";

    private String zkRoot = null; //kafka在zookeeper的存根，默认/kafkacluster
    
    private static final String DEFAULT_QUEUE_GROUP = "sigmam-queue-group"; //使用quque时，默认指定分组
    
    //消息发布者
    private KafkaMessageProducer<T> producer = null;
    
    //消息消费者
    private KafkaMessageConsumer comsumer = null;
    
    
    /**
     * kafka消息客户端
     * @param zkConnect zookeeper地址，例如:192.168.95.111:52181,192.168.95.112:52181,192.168.95.113:52181
     * @param brokerList kafka集群地址,例如:192.168.95.111:59092,192.168.95.112:59092,192.168.95.113:59092
     * @param defaultGroup 默认分组
     * @param zkRoot 在zookeeper上默认的kafka根节点
     */
    public KafkaMessageClient(String zkConnect, String brokerList, String defaultGroup, String zkRoot){
        this.zkConnect = zkConnect;
        this.brokerList = brokerList;
        this.defaultGroup = defaultGroup;
        this.zkRoot = zkRoot;
        
        this.initProducer();
        this.initConsumer();
    }

    public KafkaMessageClient(String zkConnect, String  brokerList, String defaultGroup, String zkRoot, String plainText) {
        this.kafkaMessagePlaintext  =plainText;
        this.zkConnect = zkConnect;
        this.brokerList = brokerList;
        this.defaultGroup = defaultGroup;
        this.zkRoot = zkRoot;

        this.initProducer();
        this.initConsumer();
    }

    /**
     * 初始化消息发送者
     */
    private void initProducer(){
        try{
            log.info("begin to initProducer for "+this.brokerList +"...");
            Properties props = new Properties();
//            props.put("metadata.broker.list", this.brokerlist);
            props.put("bootstrap.servers", this.brokerList);
            props.put("kafka_message_plaintext", this.kafkaMessagePlaintext);
            props.put("max.request.size", 1024*1024*10);//10M
//            props.put("serializer.class", "cn.ac.iscas.gz.bigdata.sdk.mq.kafka.encoder.KafkaJSONMessageSerializer");
            props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
            if(this.kafkaMessagePlaintext.equals("1")){
                props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
            }else{
                props.put("value.serializer", "cn.ac.iscas.gz.bigdata.sdk.mq.kafka.encoder.KafkaMessageSerializer");
            }
            this.producer = new KafkaMessageProducer<T>(props);
            log.info("initProducer  for "+this.brokerList +" successfully!");
        }catch(Throwable tr){
            log.error("initProducer  for "+this.brokerList +" catch an exception",tr);
        }
    }
    
    /**
     * 初始化消息消费者
     */
    private void initConsumer(){
        try{
            log.info("begin to initConsumer ...");
            Properties props = new Properties();
            props.put("bootstrap.servers", this.brokerList);
            props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
            if(this.kafkaMessagePlaintext.equals("1")){
                props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
            }else{
                props.put("value.deserializer", "cn.ac.iscas.gz.bigdata.sdk.mq.kafka.encoder.KafkaJSONMessageDeserializer");
            }
            props.put("enable.auto.commit", "true");
            props.put("auto.commit.interval.ms", "1000");
            String autoRestModel = System.getProperty("kafka_auto_offset_reset");
            if(autoRestModel == null || autoRestModel.trim().equals("")){
            	autoRestModel = "earliest";
            }else if((!autoRestModel.trim().equals("earliest"))
            		&& (!autoRestModel.trim().equals("latest"))
            		&& (!autoRestModel.trim().equals("none"))
            		){ //配置有误
            	autoRestModel = "earliest";
            }
            log.info("autoRestModel="+autoRestModel);
            props.put("auto.offset.reset", autoRestModel); //能获取历史消息
            
            this.comsumer = new KafkaMessageConsumer(props,this.defaultGroup);
            
            log.info("initConsumer  successfully!");
        }catch(Throwable tr){
            log.error("initConsumer catch an exception",tr);
        }
    }
    
    
    /**
     * 添加一个消息处理器到处理队列
     * @param topicName topic名称
     * @param handler 消息处理器
     */
    @Override
    public void addMessageHandler(String topicName, MessageHandler handler) throws Exception{
        try {
            this.comsumer.addMessageHandler(topicName, handler);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * 添加一个消息处理器到处理队列
     * @param topicName  topic名称
     * @param group  group名称
     * @param handler  消息处理器
     * @throws Exception
     */
    @Override
    public void addMessageHandler(String topicName,String group,MessageHandler handler) throws Exception{
        this.comsumer.addMessageHandler(topicName, group, handler);
    }
    
    /**
     * 添加一个队列消息处理器到处理队列
     * @param queueName 队列名称
     * @param handler 消息处理器
     */
    @Override
    public void addQueueMessageHandler(String queueName, MessageHandler handler) throws Exception{
        try {
            this.addMessageHandler(queueName, DEFAULT_QUEUE_GROUP, handler);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * 将消息处理器从队列中删除
     * 当某一个Handler已经不在使用的时候，请及时调用此方法，将Handler删除。
     * @param topicName topic名称 
     * @param handler
     */
    @Override
    public void removeMessageHandler(String topicName,MessageHandler handler)  throws Exception{
        try {
            this.comsumer.removeMessageHandler(topicName, handler);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * 将消息处理器从队列中删除
     * 当某一个Handler已经不在使用的时候，请及时调用此方法，将Handler删除。
     * @param topicName topic名称 
     * @param group 分组名称
     * @param handler
     */
    public void removeMessageHandler(String topicName,String group,MessageHandler handler) throws Exception{
        this.comsumer.removeMessageHandler(topicName, group, handler);
    }
    
    /**
     * 发送消息到服务器
     * @param message
     * @throws Exception
     */
    @Override
    public void send(Message message) throws Exception{
        this.send(message, false);
    }
    
    /**
     * 发送消息到服务器
     * @param topicName the specific topic
     * @param message
     * @throws Exception
     */
    @Override
    public void send(String topicName, Message message) throws Exception{
        try {
            this.producer.send(topicName, message);
        }catch (Exception e) {
            log.error("send message catch an exception",e);
            throw e;
        }
    }
    
    /**
     * 发送消息到服务器
     * @param message 消息对象
     * @param isOrdered 是否顺序发送  true:是顺序发送，false:不保证顺序
     */
    public void send(Message message,boolean isOrdered) throws Exception{
        try {
            this.producer.send(message,isOrdered);
        }catch (Exception e) {
            log.error("send message catch an exception",e);
            throw e;
        }
    }
    
    @Override
    public boolean addTopic(String dest) throws NamingException {
        // TODO Auto-generated method stub
        return false;
    }

	@Override
	public void receive() throws Exception {
		// TODO Auto-generated method stub
	}

}

