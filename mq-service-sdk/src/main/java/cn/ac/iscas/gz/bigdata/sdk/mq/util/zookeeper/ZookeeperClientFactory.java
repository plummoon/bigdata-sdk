package cn.ac.iscas.gz.bigdata.sdk.mq.util.zookeeper;

import cn.ac.iscas.gz.bigdata.sdk.common.StringUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Title:
 *   Zookeeper客户端工厂类
 * @description:
 *
 * @Company: iscas
 * @version 1.0
 */
public class ZookeeperClientFactory {
	
	//日志
	private static Logger log = LogManager.getLogger(cn.ac.iscas.gz.bigdata.sdk.mq.util.zookeeper.ZookeeperClientFactory.class);
	
	//zookeeper客户端端操作缓存
	private Map<String,ZookeeperClient> clientMap = new ConcurrentHashMap<String,ZookeeperClient>();
	
	private static cn.ac.iscas.gz.bigdata.sdk.mq.util.zookeeper.ZookeeperClientFactory instance = new cn.ac.iscas.gz.bigdata.sdk.mq.util.zookeeper.ZookeeperClientFactory();
	
	
	public static cn.ac.iscas.gz.bigdata.sdk.mq.util.zookeeper.ZookeeperClientFactory getInstace(){
		return instance;
	}
	
	private void ZookeeperClientFactory(){
	}
	
	/**
	 * 获取zookeeper客户端操作对象
	 * @param namespace
	 * @return
	 */
	public ZookeeperClient getZKClient(String namespace){
		String zkConnect = System.getProperty("zkconnect");
		return this.getZKClient(zkConnect, namespace);
	}
	
	
	/**
	 * 获取zookeeper客户端操作对象
	 * @param zk_connect
	 * @param namespace
	 * @return
	 */
	public ZookeeperClient getZKClient(String zk_connect,String namespace){
		if(StringUtils.isEmpty(zk_connect)){
			throw new RuntimeException("zk_connect can't be null");
		}
		
		if(StringUtils.isEmpty(namespace)){
			throw new RuntimeException("namespace can't be null");
		}
		
		String key = this.getKey(zk_connect, namespace);
 		if(!this.clientMap.containsKey(key)){ //如果之前没创建过
 			this.buildZKClient(zk_connect, namespace);
		}
		
		return this.clientMap.get(key);
	}
	
	
	/**
	 * 构建Zookeeper客户端
	 * @param zk_connect
	 * @param namespace
	 */
	private synchronized void buildZKClient(String zk_connect,String namespace){
		try {
			ZookeeperClient zkClient =  new ZookeeperClient(zk_connect, namespace);
			String key = this.getKey(zk_connect, namespace);
			this.clientMap.put(key, zkClient);
		} catch (Throwable tr) {
			log.error("buildZKClient zk_connect="+zk_connect+",namespace="+namespace+" catch an exception",tr);
			throw new RuntimeException(tr);
		}
		
	}
	
	
	/**
	 * 组装key值
	 * @param zk_connect
	 * @param namespace
	 * @return
	 */
	private String getKey(String zk_connect,String namespace){
		String key = zk_connect+"/"+namespace;
		key = key.toLowerCase();
		return key;
	}

}
