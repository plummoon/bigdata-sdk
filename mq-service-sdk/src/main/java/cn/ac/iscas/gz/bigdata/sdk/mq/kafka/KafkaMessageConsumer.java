package cn.ac.iscas.gz.bigdata.sdk.mq.kafka;

import cn.ac.iscas.gz.bigdata.sdk.common.StringUtils;
import cn.ac.iscas.gz.bigdata.sdk.mq.Message;
import cn.ac.iscas.gz.bigdata.sdk.mq.MessageHandler;
import cn.ac.iscas.gz.bigdata.sdk.mq.kafka.encoder.KafkaJSONMessageDeserializer;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @Title:
 *  Kafka消息消费者
 * @description:
 *  负责Kafka消息的消费
 * @Company: iscas
 * @version 1.0
 */
public class KafkaMessageConsumer {
    
    //日志
    private static final Logger log = LogManager.getLogger(cn.ac.iscas.gz.bigdata.sdk.mq.kafka.KafkaMessageConsumer.class);
    
    
    public static final char[] INVALID_GROUP_CHAR = { '~', '!', '#', '$', '%', '^', '&', '*', '(', ')', '+', '=', '`', '\'',
                                                      '"', ',', ';', '/', '?', '[', ']', '<', '>', '.', ':', ' ' };
    
    //默认消息分组
    private String defaultGroup = null;
    
    //配置参数
    private Properties defaultProps = null;
    
    //默认建立5个分区,也就是对应最多可以用5个线程进行拉取
    private int numThreadsPreTopic = 5;
    
    //保存已经建立的topic_group对应的consumer key=topic_group,value=ConsumerConnector
    private static Map<String,Consumer> topicGroupConsumerMap = new ConcurrentHashMap<String,Consumer>();
     
    //保存已经注册的MessageHandler key=topic_group,value=MetaqMessageListenerWrapper
    private static Map<String,CopyOnWriteArraySet<MessageHandler>>  topicGroupHandlerMap = new ConcurrentHashMap<String,CopyOnWriteArraySet<MessageHandler>>();

    
    /**
     * @param defaultProps 配置参数
     * @param defaultGroup 默认分组
     * @throws Exception
     */
    public KafkaMessageConsumer(Properties defaultProps,String defaultGroup) throws Exception{
        if(defaultProps == null || defaultProps.size() <= 0){
            throw new RuntimeException("defaultProps can't be null");
        }
        
        if(StringUtils.isEmpty(defaultGroup)){
            throw new RuntimeException("defaultGroup can't be null");
        }
        
        this.defaultProps = defaultProps;
        this.defaultGroup = defaultGroup;
        this.defaultGroup = this.processInvalidGroupChar(this.defaultGroup);
    }
    
    
    /**
     * 添加一个消息处理器到处理队列
     * @param topicName topic名称
     * @param handler 消息处理器
     */
    public void addMessageHandler(String topicName, MessageHandler handler) throws Exception{
        this.addMessageHandler(topicName,this.defaultGroup, handler);
    }
    
    /**
     * 添加一个消息处理器到处理队列
     * @param topicName  topic名称
     * @param group  group名称
     * @param handler  消息处理器
     * @throws Exception
     */
    public synchronized void addMessageHandler(String topicName,String group,MessageHandler handler) throws Exception{
        String newGroup = this.processInvalidGroupChar(group);
        String key = this.getTopicGroupKey(topicName, newGroup);
        if(!cn.ac.iscas.gz.bigdata.sdk.mq.kafka.KafkaMessageConsumer.topicGroupConsumerMap.containsKey(key)){ //不包含topic,group,则新建该topic,group的consumer
            this.createComsumer(topicName, newGroup);
        }
        
        CopyOnWriteArraySet<MessageHandler> handlers = topicGroupHandlerMap.get(key);
        handlers.add(handler);
    }
    
    /**
     * 将消息处理器从队列中删除
     * 当某一个Handler已经不在使用的时候，请及时调用此方法，将Handler删除。
     * @param topicName topic名称 
     * @param handler
     */
    public void removeMessageHandler(String topicName,MessageHandler handler) throws Exception{
        this.removeMessageHandler(topicName,this.defaultGroup, handler);
    }
    
    /**
     * 将消息处理器从队列中删除
     * 当某一个Handler已经不在使用的时候，请及时调用此方法，将Handler删除。
     * @param topicName topic名称 
     * @param group 分组名称
     * @param handler
     */
    public void removeMessageHandler(String topicName,String group,MessageHandler handler) throws Exception{
        String newGroup = this.processInvalidGroupChar(group);
        String key = this.getTopicGroupKey(topicName, newGroup);
        if(!topicGroupHandlerMap.containsKey(key)){
            return;
        }
        
        CopyOnWriteArraySet<MessageHandler> handlers = topicGroupHandlerMap.get(key);
        if(handlers.contains(handler)){
            handlers.remove(handler);
        }
    }
    
    /**
     * 构建Consumer
     * @param topicName topic名称
     * @param group  topic消费者分组
     */
    public void createComsumer(String topicName,String group){
        log.info("begin kafka createComsumer for topicName="+topicName+",group="+group);
        TopicManager.getInstance().createTopicIfNotExist(topicName);
        
        String key = this.getTopicGroupKey(topicName, group);
        
        Properties props = new Properties();
        props.putAll(this.defaultProps);
        props.put("group.id", group);
        
        log.info("createComsumer props="+props.toString());
        KafkaConsumer<Long,Message> consumer = new KafkaConsumer<Long, Message>(props);
        consumer.subscribe(Arrays.asList(topicName));

        // now launch all the threads
        ExecutorService executor = Executors.newFixedThreadPool(numThreadsPreTopic);

        int threadNumber = 1;
        executor.submit(new ConsumerMsgTask(topicName,group,consumer,threadNumber));

        cn.ac.iscas.gz.bigdata.sdk.mq.kafka.KafkaMessageConsumer.topicGroupConsumerMap.put(key, consumer);
        CopyOnWriteArraySet<MessageHandler> handlers = new CopyOnWriteArraySet<MessageHandler>();
        cn.ac.iscas.gz.bigdata.sdk.mq.kafka.KafkaMessageConsumer.topicGroupHandlerMap.put(key, handlers);
    }
    
    
    /**
     * 处理分组中特殊字符
     * @param defaultGroup
     * @return
     */
    private String processInvalidGroupChar(final String defaultGroup){
        StringBuffer sb = new StringBuffer();
        try{   
            for(int i = 0; i < defaultGroup.length(); i++){
                char c = defaultGroup.charAt(i);
                boolean isInvalidChar = false;
                for(char invalid : INVALID_GROUP_CHAR){
                    if(c == invalid){
                        isInvalidChar = true;
                        sb.insert(i, "_");
                        break;
                    }
                }
                
                if(isInvalidChar == false){
                    sb.insert(i, c);
                }
            }
            
        }catch(Exception ex){
            log.error("processInvalidGroupChar "+defaultGroup+" catch an exception",ex);
            return defaultGroup;
        }
        
        return sb.toString();
    }
        
    
    /**
     * 构建topicGroupMap的key
     * @param topicName
     * @param group
     * @return
     */
    private String getTopicGroupKey(String topicName,String group){
        String key = topicName+"_"+group;
        return key;
    }
    
    
    /**
     * 消费者消息任务
     * @author lnj2050
     */
    class ConsumerMsgTask implements Runnable {
        private final AtomicBoolean closed = new AtomicBoolean(false);
        private final KafkaConsumer consumer;
        private String topicName;
        private String group;
        private int threadNumber;
        //序列化器
        private KafkaJSONMessageDeserializer serializer = null;

        public ConsumerMsgTask(String topicName,String group,KafkaConsumer consumer, int threadNumber) {
            this.topicName = topicName;
            this.group = group;
            this.threadNumber = threadNumber;
            this.consumer = consumer;
            this.serializer = new KafkaJSONMessageDeserializer();
        }
        public void run() {
            try {
                consumer.subscribe(Arrays.asList(topicName));
                while (!closed.get()) {
                    ConsumerRecords<Long,Message> consumerRecords = consumer.poll(Duration.ofMillis(10000));
                    Iterator consumerRecordsIt = consumerRecords.iterator();
                    while (consumerRecordsIt.hasNext()){
                        Object ob = consumerRecordsIt.next();
                        System.out.println(ob.toString());
                    }
                    for (ConsumerRecord<Long, Message> record : consumerRecords) {
                        Message msgBean = record.value();
                        this.dispatchMessageToHandlers(msgBean);
                    }
                }
            }
            catch (WakeupException e) {
                // Ignore exception if closing
                if (!closed.get()) throw e;
            } finally {
                consumer.close();
            }
            log.warn("Shutting down  topicName="+this.topicName+",group="+this.group+" Thread: " + this.threadNumber);
        }

        // Shutdown hook which can be called from a separate thread
        public void shutdown() {
            closed.set(true);
            consumer.wakeup();
        }
        /**
         * 派发消息到各个MessageHandler中
         * @param message
         */
        public void dispatchMessageToHandlers(Message message){
            String key = getTopicGroupKey(this.topicName,this.group);
            CopyOnWriteArraySet<MessageHandler> handlers = topicGroupHandlerMap.get(key);
            if(handlers == null || handlers.size() <= 0){
                return;
            }
            
            Object[]arr = handlers.toArray();
            for(int i = 0; i < arr.length; i++){
                MessageHandler handler = (MessageHandler)arr[i];
                if(handler == null){
                    continue;
                }
                
                String handlerClassName = handler.getClass().getName();
                try{
                    long begin = System.currentTimeMillis();
                    handler.onMessage(message);
                    long during = System.currentTimeMillis() - begin;
                    
                }catch(Throwable tr){
                    log.error("handler ["+handlerClassName+"] on message catch an exception",tr);
                }
            }
        }
    }

}

