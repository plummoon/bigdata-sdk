package cn.ac.iscas.gz.bigdata.sdk.mq;


import cn.ac.iscas.gz.bigdata.sdk.common.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @Title:
 *  消息接收模拟器
 * @description:
 *
 */
public class RecieveMsgSimulator {
    /**  日志文件 */
    private Logger log = LogManager.getLogger(RecieveMsgSimulator.class);
    
    public static long begin = 0;
    public static long during  =0;
    public static long count = 0;
    
    public RecieveMsgSimulator(){
        MessageClient.getInstance();
    }
    
	public void registMessageHandler(final String topic){
		try{
			MessageClient.getInstance().addMessageHandler(topic, msg -> {
				try{
					if(count == 0){
						begin = System.currentTimeMillis();
					}
					count++;
					during = System.currentTimeMillis() - begin;
					if(count % 10 == 0){
						double speed = (double)count*1000/during;
						log.info("recieved:count="+count+",during="+during+"ms,speed="+speed+"条/s");
					}
					if(msg == null){
						log.error("msg is null");
					}else{
						log.info("recieved a message "+msg.toString());
					}
				}catch(Throwable tr){
					tr.printStackTrace();
				}
			});
			System.out.println("setup messsage listener topic="+topic+" completely!");
		}catch(Throwable tr){
			tr.printStackTrace();
		}
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.setProperty("mq-defaultgroup","test1");
	    RecieveMsgSimulator test = new RecieveMsgSimulator();
		test.registMessageHandler("test");
	}
}
