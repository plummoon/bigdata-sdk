package cn.ac.iscas.gz.bigdata.sdk.mq;

import cn.ac.iscas.gz.bigdata.sdk.mq.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @Title:
 *  消息发送模拟器
 * @description:
 *
 */
public class SendMsgSimulator {
    /**  日志文件 */
    private static Logger log = LogManager.getLogger(cn.ac.iscas.gz.bigdata.sdk.mq.SendMsgSimulator.class);
    private static ObjectMapper objectMapper;
    private ExecutorService pool = null;
    
//    private List<Message> list = new ArrayList<Message>();
    
    public SendMsgSimulator(int poolSize){
        objectMapper = new ObjectMapper();
        MessageClient.getInstance();
        pool = Executors.newFixedThreadPool(poolSize);
    }


    public static void main(String[] args) {
        int count = Integer.parseInt(args[0]);
        int poolSize = Integer.parseInt(args[1]);
        cn.ac.iscas.gz.bigdata.sdk.mq.SendMsgSimulator test = new cn.ac.iscas.gz.bigdata.sdk.mq.SendMsgSimulator(poolSize);
        log.info("begin to send count="+count+",poolSize="+poolSize);
        test.sendMsg(count);
    }
    
    public void sendMsg(int count){
        try{
            long begin = System.currentTimeMillis();
           
            for(int i = 0;i < count; i++){
                final Message message = new Message();
                Object userObj = this.buildMsgData(i);
                //String userObj = "message mfdalfjdalfdafjda;lfl;dafdafl;djafl;dafdlfdalfda;fl"+i;
//                message.setUserObject(userObj);
                message.setUserObject(userObj);
                message.setMessageKey("key0021");
                message.setCompress(true);
                message.setTopic("test112");
//                list.add(message);
                Future f = pool.submit(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            //log.info("");
                            MessageClient.getInstance().send("test112", message);
                            log.info("send message "+message.getUserObject().toString()+" sucessfully!");
                        } catch (Exception e) {
                        }
                    }
                    
                });
            }
            
            while(threadStatus()){
                break;
            }
            
            long during = System.currentTimeMillis() - begin;
            double speed = (double)count*1000/during;
            log.info("sendMsg count="+count+",during="+during+",speed="+speed+"条/s");
        }catch(Throwable tr){
            tr.printStackTrace();
        }
    }
    
    public void sendOrderedMsg(int count){
        try{
            long begin = System.currentTimeMillis();
           
            for(int i = 0;i < count; i++){
                final Message message = new Message();
                Object userObj = this.buildMsgData(i);
                //String userObj = "message mfdalfjdalfdafjda;lfl;dafdafl;djafl;dafdlfdalfda;fl"+i;
                message.setUserObject(userObj);
                message.setTopic("test");
//                list.add(message);
                Future f = pool.submit(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            //log.info("");
                            MessageClient.getInstance().send(message,true);
                            log.info("send message "+message.toString()+" sucessfully!");
                        } catch (Exception e) {
                        }
                    }
                    
                });
            }
            
            while(threadStatus() == true){
                break;
            }
            
            long during = System.currentTimeMillis() - begin;
            double speed = (double)count*1000/during;
            log.info("sendMsg count="+count+",during="+during+",speed="+speed+"条/s");
        }catch(Throwable tr){
            tr.printStackTrace();
        }
    }
    
    /**
     * 构建消息数据
     * @param i
     * @return
     */
    private String buildMsgData(int i) throws JsonProcessingException {
        List<User> userList = new ArrayList<>(2);
        User user1 = new User(1000+i, "张三", null, 7777.88F);
        User user2 = new User(2000+i, "李四", new Date(), 9800.78F);
        userList.add(user1);
        userList.add(user2);
        Iterator<User> it = userList.iterator();
        String userObj = "";
        while (it.hasNext()) {
            userObj += objectMapper.writeValueAsString(it.next());
            userObj += "\r\n";
        }
        return  userObj;
    }
    
    //判断线程池是否结束 
    public boolean threadStatus(){
        pool.shutdown();
        while(true){
            if(pool.isTerminated()){
                return true;
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                // TODO 自动生成 catch 块
                e.printStackTrace();
            }
        }
    }
    
    private Message createMessage(int i) throws JsonProcessingException {
        Message msg = new Message();
        msg.setType(600001); //新增用户
        msg.setUserObject(this.buildMsgData(i));
        msg.setTopic("test111");
        return msg;
    }

}
